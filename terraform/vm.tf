# Create virtual machine
module "vm" {
  source = "git::https://gitlab.com/azure-iac2/root-modules.git//vm"
  vm_name              = var.vm_name  
  business_divsion     = var.business_divsion
  environment          = var.environment
  resource_group_name  = data.azurerm_resource_group.rg.name
  location             = data.azurerm_resource_group.rg.location
  nic_id               = data.azurerm_network_interface.nic.id
  vm_size              = var.vm_size
  computer_name        = var.computer_name
  admin_username       = var.admin_username
  admin_password       = var.admin_password  
}

data "azurerm_virtual_machine" "data_vm" {
  name                 = "${var.business_divsion}-${var.environment}-${var.vm_name}" 
  resource_group_name  = data.azurerm_resource_group.rg.name
}

resource "azurerm_virtual_machine_extension" "vm_extention" {
  name                  = var.computer_name
  virtual_machine_id    = data.azurerm_virtual_machine.data_vm.id
  publisher             = "Microsoft.Azure.Extensions"
  type                  = "CustomScript"
  type_handler_version  = "2.0"

  settings = <<SETTINGS
    {
        "script": "${base64encode(templatefile("custom_script.sh", {
          vmname="${data.azurerm_virtual_machine.data_vm.name}"
        }))}"
    }
  SETTINGS

  tags =  {
    environment = var.environment
  }

  depends_on = [data.azurerm_virtual_machine.data_vm]
}