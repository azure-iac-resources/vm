data "azurerm_resource_group" "rg"{
  name = var.resource_group_name
}

data "azurerm_network_interface" "nic" {
  name                = var.nic_name
  resource_group_name = var.resource_group_name
}